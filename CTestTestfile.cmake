# CMake generated Testfile for 
# Source directory: /root/varcoin-0.1.6
# Build directory: /root/varcoin-0.1.6
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(external)
subdirs(src)
subdirs(tests)
