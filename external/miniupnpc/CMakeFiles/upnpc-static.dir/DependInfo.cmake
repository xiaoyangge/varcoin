# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/root/varcoin-0.1.6/external/miniupnpc/connecthostport.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/connecthostport.c.o"
  "/root/varcoin-0.1.6/external/miniupnpc/igd_desc_parse.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/igd_desc_parse.c.o"
  "/root/varcoin-0.1.6/external/miniupnpc/minisoap.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/minisoap.c.o"
  "/root/varcoin-0.1.6/external/miniupnpc/minissdpc.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/minissdpc.c.o"
  "/root/varcoin-0.1.6/external/miniupnpc/miniupnpc.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/miniupnpc.c.o"
  "/root/varcoin-0.1.6/external/miniupnpc/miniwget.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/miniwget.c.o"
  "/root/varcoin-0.1.6/external/miniupnpc/minixml.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/minixml.c.o"
  "/root/varcoin-0.1.6/external/miniupnpc/portlistingparse.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/portlistingparse.c.o"
  "/root/varcoin-0.1.6/external/miniupnpc/receivedata.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/receivedata.c.o"
  "/root/varcoin-0.1.6/external/miniupnpc/upnpc.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/upnpc.c.o"
  "/root/varcoin-0.1.6/external/miniupnpc/upnpcommands.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/upnpcommands.c.o"
  "/root/varcoin-0.1.6/external/miniupnpc/upnperrors.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/upnperrors.c.o"
  "/root/varcoin-0.1.6/external/miniupnpc/upnpreplyparse.c" "/root/varcoin-0.1.6/external/miniupnpc/CMakeFiles/upnpc-static.dir/upnpreplyparse.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "MINIUPNPC_SET_SOCKET_TIMEOUT"
  "_BSD_SOURCE"
  "_POSIX_C_SOURCE=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "src"
  "external"
  "version"
  "src/Platform/Linux"
  "src/Platform/Posix"
  "/usr/local/include"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
