// Copyright (c) 2012-2018, The CryptoNote developers, YxomTech
//
// This file is part of Varcoin.
//
// Varcoin is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Varcoin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Varcoin.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

namespace VarNote
{
  const static boost::uuids::uuid Varcoin_NETWORK = { { 0x7b ,0x2c, 0x1f, 0x4d , 0x16, 0xa0 , 0x00, 0x5b, 0x20, 0xe1, 0xff, 0x13, 0x8f, 0x15, 0x54, 0x99} }; //Bender's nightmare
}
